@extends('adminlte.master')

@section('content')
<div class="card card-primary">
            <div class="card-header with-border">
              <h3 class="card-title">Buat Pertanyaan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <form role="form" action="/pertanyaan" method="POST">
            @csrf
              <div class="card-body">
                <div class="form-group">
                  <label for="title">Judul Pertanyaan</label>
                  <input type="text" class="form-control" id="judul" value=" {{old('judul',' ')}}"name="judul" placeholder="Enter judul">
                  @error('judul')
                     <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="body">Isi Pertanyaan</label>
           <!--        <input type="text" class="form-control" id="isi" value=" {{old('isi',' ')}}" name="isi" placeholder="isi"> -->
                    <textarea name="isi" class="form-control my-editor" id="isi">
                          {{old('isi',' ')}}
                    </textarea>
                  @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <input type="hidden" name="users_id" value="{{Auth::user()->id}}">
              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
</div>
@endsection
@push('script')
<script type="text/javascript">
    var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
@endpush