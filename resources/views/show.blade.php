@extends('adminlte.master')

@section('content')


          <!-- Box Comment -->
          <div class="card card-widget">
            <div class="card-header with-border">
              <div class="user-block">
                <img class="img-circle" src="{{asset('/adminlte/dist/img/user2-160x160.jpg')}}" alt="User Image">
                <span class="username"><a href="#">author : {{$pertanyaan->user->name}}</a></span>
                <span class="description"></span>
              </div>
              <!-- /.user-block -->
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-toggle="tooltip" title="" data-original-title="Mark as read">
                  <i class="fa fa-circle-o"></i></button>
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
              <!-- /.box-tools -->
         
            <!-- /.box-header -->
            <div class="card-body">
              <!-- post text -->
              <h3>{{$pertanyaan->judul}}</h3>
              <p>{!! $pertanyaan->isi !!}</p> 
              
  
              <!-- Social sharing buttons
              <button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button>
              <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button>
              <span class="pull-right text-muted">45 likes - 2 comments</span>
            </div>
             -->

            <!-- /.box-body -->
            <div class="card-footer card-comments">
            @foreach($pertanyaan->jawaban as $jawaban)
              <div class="card-comment">
                <!-- User image -->
                <img class="img-circle img-sm" src="{{asset('/adminlte/dist/img/user2-160x160.jpg')}}" alt="User Image">

                <div class="comment-text">
                      <span class="username">
                        {{$jawaban->user->name}}
                        <span class="text-muted pull-right">8:03 PM Today</span>
                      </span><!-- /.username -->
                  {{$jawaban->isi}}
                  <!-- Si Pembuat jawaban bisa delete jawabnya sendiri -->
                  @if($jawaban->user->id == Auth::user()->id)
                  <span class="username pt-2">
                  	<a href="/jawaban/{{$jawaban->id}}-{{$pertanyaan->id}}/delete" onclick="return confirm('Yakin Jawaban Anda Di Hapus?')" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                  </span>
                  @endif
                </div>
                <!-- /.comment-text -->
              </div>
              <!-- /.box-comment -->
              <div class="card-comment">
                <!-- User image 
                <img class="img-circle img-sm" src="./public" alt="User Image">
-->
              

                @endforeach
              </div>
              <!-- /.box-comment -->
            </div>
            <!-- /.box-footer -->
            <div class="card-footer">
              <form action="/jawaban/{{$pertanyaan->id}}" method="post">
              @csrf
                <img class="img-responsive img-circle img-sm" src="{{asset('/adminlte/dist/img/user2-160x160.jpg')}}" alt="Alt Text">
                <!-- .img-push is used to add margin to elements next to floating images -->
                <div class="img-push pb-3">
                  <input type="text" class="form-control input-sm" name="isi" placeholder="Press enter to post comment">
                </div>
                <div class="ml-5">
                <input type="submit" value="jawab" class="btn btn-sm btn-primary">
              	</div>
              </form>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>

@endsection