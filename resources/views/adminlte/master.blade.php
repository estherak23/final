<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>E- Forum</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('/adminlte/plugins/fontawesome-free/css/all.min.css')}} ">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('/adminlte/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Data Tables -->
  <link rel="stylesheet" href=".{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar mengambil dari file lain -->
  
  @include('adminlte.partial.navbar')

  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('adminlte.partial.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
@yield('content')
   
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>

<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2020-2021 iqbal kurniawan dan esther argyanti.</strong> All rights
    reserved.
</footer>

<!-- ./wrapper -->

<!-- Modal Edit Profile-->
<div class="modal" tabindex="-1" role="dialog" id="contain-mdl-profile">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Profile</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="elemen-form">
          <div class="form-group">
            <input type="hidden" name="id" value="{{Auth::user()->id}}">
            <label>Email</label>
            <input type="text" name="email" class="form-control" value="{{Auth::user()->email}}">
         
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control" value="{{Auth::user()->name}}">
          </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn-update-profile">Simpan </button>

      </div>
      </div>
      <div id="pesan">
        
      </div>
    </div>
  </div>
</div>
<!-- End Modal Edit Profile-->

<!-- jQuery -->
<script src="{{asset('/adminlte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('/adminlte/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('/adminlte/dist/js/demo.js')}}"></script>
<!-- Editor  -->
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('a[name="cek"]').click(function(e){
        e.preventDefault();
        $('#contain-mdl-profile').modal('show');
        
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });

        $('#btn-update-profile').click(function(e){
            var id = $('input[name="id"]').val();
            var email = $('input[name="email"]').val();
            var nama = $('input[name="nama"]').val();
            console.log(email+'sambung..'+nama);
            
            $.ajax({
              type:'POST',
              data : {
                  id:id,
                  email:email,
                  name:nama
              },
              url:'/profile/edit_profile',
              beforeSend:function(){
              },
              success:function(data){;
                $('#elemen-form').hide();
                $('#pesan').show();
                $('#pesan').html('<div class="alert alert-success" id="content_pesan" style="margin-top:2%;">'+data.success+'</div>');
                setTimeout(function(){
                    $('#pesan').hide();
                    $('#contain-mdl-profile').modal('hide');
                    $('#elemen-form').show();
                }, 1000);
              }
            });
        });
    });

});
</script>

<!--untuk js nya-->
@stack('script')
@include('sweetalert::alert')

</body>
</html>
