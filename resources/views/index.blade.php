@extends('adminlte.master')

@section('content')



<div class="card">
            <div class="card-header with-border">
              <h3 class="card-title">    
              <i class="nav-icon fas fa-th"></i>
                 Questions and Answes
              </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
          <!-- Comment dulu karena menggunakan sweat alert 
            @if(session('success'))
            <div class='alert alert-success'>
                {{session('success')}}
            </div>
            @endif 
          -->
            <a class="btn btn-info" href="/pertanyaan/create">Buat Pertanyaan</a>
            <br><br>
              <table class="table table-bordered mt-4" id="example1">
                <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Judul Pertanyaan</th>
                  <th>Isi Pertanyaan</th>
                  <th>Author</th>
                  <th style="width: 40px">Action</th>
                </tr>
                </thead>
               <tbody>
                  @foreach($pertanyaan as $key => $pertanyaan)
                  <tr>
                  
                  <td>{{$key +1 }}</td>
                  <td>{{$pertanyaan ->judul}}</td>
                  <td>{!! $pertanyaan ->isi !!}</td>
                  <td>{{ $pertanyaan->user->name }}</td>
                  <td style="display: flex;">
                  <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info btn-sm">Detail</a>
                  <!-- Jika dia pembuat perntayaan maka bisa update dan delete -->
                  @if($pertanyaan->users_id == Auth::user()->id)
                  <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-info btn-sm">Edit</a>
<!-- <form action="/pertanyaan/{{$pertanyaan->id}}" method="pertanyaan"> -->
<form action="/pertanyaan/{{$pertanyaan->id}}" method="post">
@csrf
@method('DELETE')
<input type="submit" value="Delete" onclick="return confirm('Yakin Pertanyaan Anda mau dihapus?')" class="btn btn-danger btn-sm">
</form>
@endif

                  </td>
                  </tr>
                 
                  
                @endforeach
              </tbody>
              </table>
            </div>
            <!-- /.card-body -->
           







@endsection
@push('script')
<script src="{{asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/adminlte/plugins/datatables/jquery.datatables.js')}}"></script>
<script src="{{asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript">

  $(function(){
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });

</script>
@endpush