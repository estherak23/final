<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});


route::resource('pertanyaan','PertanyaanController');
Route::post('/profile/edit_profile','ProfileController@edit_profile');
Route::post('/profile/edit_password','ProfileController@edit_password');
/*
 Route::get('/profile',function(){
 	return view('profile.index');
 });
*/

//controller untuk jawaban
route::post('/jawaban/{pertanyaan_id}','JawabanController@store');
Route::get('/jawaban/{id}/delete','JawabanController@destroy');

Route::get('/logout', 'HomeController@logout');

Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
