<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use App\pertanyaan;
use App\Jawaban;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index()
    {
        $pertanyaan= pertanyaan::all();
        return view('index',compact('pertanyaan'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $request->validate([

            'judul'=> 'required|:posts',
            'isi'=> 'required'
        
        
        ]) ;
//$pertanyaan= new pertanyaan;
//$pertanyaan->judul =$request["judul"];
//$pertanyaan->isi =$request["isi"];
//$pertanyaan->save();

$pertanyaan = pertanyaan::create([
    "judul"=>$request["judul"],
    "isi"=>$request["isi"],
    "users_id" =>$request["users_id"]
    ]);
    
    Alert::success('Berhasil', 'Pertanyaan Berhasil Di Simpan');
    return redirect('/pertanyaan')->with('success','post berhasil disimpan');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cek = 'test 123';
        $pertanyaan= pertanyaan::find($id);
       
        return view ('show',compact('pertanyaan'));
        // var_dump($cek);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pertanyaan= pertanyaan::find($id);


        return view ('edit',compact('pertanyaan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update=pertanyaan::where('id',$id)->update([


            "judul"=>$request["judul"],
            "isi"=>$request["isi"],     
            "users_id" =>$request["users_id"]
        ]); 

        Alert::success('Berhasil', 'Pertanyaan Berhasil Di Simpan');
        return redirect('/pertanyaan')->with('success','berhasil update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        jawaban::where('pertanyaan_id', $id)->delete();
        pertanyaan::destroy($id); 
        Alert::success('Berhasil', 'Pertanyaan Berhasil Di Hapus');
        return redirect ('/pertanyaan')->with('success','berhasil dihapus');
    }



}
