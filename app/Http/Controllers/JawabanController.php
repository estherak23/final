<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\pertanyaan;
use App\Jawaban;
use Auth;

class JawabanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($pertanyaan_id,Request $request)
    {
        $pertanyaan=pertanyaan::find($pertanyaan_id);
        
        $jawaban=new jawaban;
        $jawaban->isi=$request['isi'];
        $jawaban->pertanyaan_id=$pertanyaan_id;
        $jawaban->users_id= Auth::user()->id;
        $jawaban->save();
        
        Alert::success('Berhasil', 'Jawaban Berhasil Di Simpan');
        return redirect()->route('pertanyaan.show',['pertanyaan'=> $pertanyaan_id]);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $explode_id = explode('-',$id);
        Jawaban::destroy($explode_id[0]);
        Alert::success('Berhasil', 'Jawaban Berhasil Di Hapus');
        return redirect()->route('pertanyaan.show',['pertanyaan'=> $explode_id[1]]);
        // echo $explode_id[0];
    }
}
