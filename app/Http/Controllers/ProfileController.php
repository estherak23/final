<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\User;

class ProfileController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function edit_profile(Request $request){
    	$editUser = User::where('id','=',$request->id)->first();
    	$cek = $editUser->update([
    						'email' => $request->email,
    						'name' => $request->name
    					   ]
    					);
        return response()->json(['success'=>'Profile Berhasil di Update']);
    }
  
    public function edit_password(Request $request){
    	$editUser = User::where('id','=',$request->id)->first();
    	$cek = $editUser->update(['password' => bcrypt($request->password)]);
        return response()->json(['success'=>'Profile Berhasil di Update']);
    }

}
