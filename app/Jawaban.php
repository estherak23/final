<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    protected $table = 'jawaban';
protected $guarded=[];

      
public function pertanyaan(){

    return $this->belongsTo('App\pertanyaan','pertanyaan_id');
    
    }

    //fungsi untuk jawaban itu milik user siapa
    public function user(){

        return $this->belongsTo('App\User','users_id');
    }


    
//many to many relation untuk like dislike jawaban
    public function likeDislikeJawaban(){

        return $this->belongsToMany('App\likeDislikeJawaban');
        
        }

}
