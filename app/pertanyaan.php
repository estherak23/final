<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\pertanyaan;
use Auth;

class pertanyaan extends Model
{
    protected $table = 'pertanyaan';
    public $timestamps = false;
    
    protected $fillable=["judul","isi","users_id"];
    // Harus nya paaki s tadi users_id bukan user_id


    //pertanyaan dimiliki oleh user
    public function user()
    {
        // return $this->belongsTo('App\User','user_id');
        return $this->belongsTo('App\User','users_id');
    }
    


//1 pertanyaan punya banyak jawaban
public function jawaban(){

return $this->hasMany('App\Jawaban','pertanyaan_id');

}



//one to one
public function jawaban_tepat(){

    return $this->belongsTo('App\pertanyaan','jawaban_tepat_id');
    
    }




//many to many relation 
public function likeDislikePertanyaan(){

    return $this->belongsToMany('App\likeDislikePertanyaan');
    
    }
}
